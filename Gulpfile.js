var gulp = require("gulp");
var sass = require("gulp-sass");
var sourcemaps = require("gulp-sourcemaps");
var prefix = require('gulp-autoprefixer');

// var sassLint = require('gulp-sass-lint');

var prefixerOptions = {
  overrideBrowserslist: ['last 2 versions']
};

gulp.task("styles", function() {
 return gulp
    .src("./scss/**/*.scss")
    .pipe(sourcemaps.init())
    .pipe(sass({ outputStyle: "expanded" }).on("error", sass.logError))
    .pipe(sourcemaps.write())
    .pipe(prefix(prefixerOptions))
    .pipe(gulp.dest("./css/"));
});

//Watch task
gulp.task("watch", function() {
  return gulp.watch("scss/**/*.scss", gulp.series('sass'));
});
gulp.task('sass', function () {
  return gulp.src('./sass/**/*.scss')
      .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
      .pipe(gulp.dest('./css'));
});